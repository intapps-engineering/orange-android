package com.zabihah.ui.data.api;

import com.zabihah.ui.data.api.responses.AuthenticationResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;


interface ApiService {
    @POST("/mem_signin.php")
    Call<AuthenticationResponse> authenticateUser(@QueryMap Map<String, String> dataMap);
}
