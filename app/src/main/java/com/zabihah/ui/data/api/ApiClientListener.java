package com.zabihah.ui.data.api;

import com.zabihah.ui.data.api.responses.DefaultResponse;

public class ApiClientListener {
    public interface AccountRegistrationListener {
        void onAccountRegistered(DefaultResponse defaultResponse);
    }
}
