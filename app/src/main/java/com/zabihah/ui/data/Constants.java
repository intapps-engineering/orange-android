package com.zabihah.ui.data;

public class Constants {
    public static String page = "page";
    public static String member = "member";
    public static String latitude = "latitude";
    public static String zabihah = "zabihah";
    public static String longitude = "longitude";
    public static String isUserSignedIn = "is-user-signed-in";
    public static String memberFirstName = "member-first-name";
    public static String memberLastName = "member-last-name";
    public static String memberAddress = "member-address";
    public static String memberPhoto = "member-photo";
}

