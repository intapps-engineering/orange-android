package com.zabihah.ui.data;

public interface PermissionRequestListener {
    void onPermissionGranted();
}
