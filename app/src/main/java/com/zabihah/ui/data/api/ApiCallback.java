package com.zabihah.ui.data.api;

import retrofit2.Response;

public interface ApiCallback<T> {
    /**
     * Invoked for a received HTTP response.
     *
     * <p>Note: An HTTP response may still indicate an application-level failure such as a 404 or 500.
     * Call {@link Response#isSuccessful()} to determine if the response indicates success.
     */
    void onResponse(T responseObject, Response<T> response);

    /**
     * Invoked when a network exception occurred talking to the server or when an unexpected exception
     * occurred creating the request or processing the response.
     */
    void onFailure(T responseObject, Throwable t);

    void onFailure(T responseObject, Response<T> response);
}
