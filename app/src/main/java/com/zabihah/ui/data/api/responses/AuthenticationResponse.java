package com.zabihah.ui.data.api.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AuthenticationResponse extends DefaultResponse {
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("emailAddress")
    @Expose
    private String emailAddress;
    @SerializedName("member")
    @Expose
    private Member member;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }
}
