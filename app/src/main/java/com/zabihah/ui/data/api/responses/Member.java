package com.zabihah.ui.data.api.responses;

import androidx.annotation.Nullable;

import com.google.gson.JsonElement;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.zabihah.ui.data.api.Exclude;

import java.io.Serializable;

import kotlin.jvm.Transient;

public class Member implements Serializable {
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public String getMemHash() {
        return memHash;
    }

    public void setMemHash(String memHash) {
        this.memHash = memHash;
    }

    public Integer getAnon() {
        return anon;
    }

    public void setAnon(Integer anon) {
        this.anon = anon;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayNameFirst() {
        return displayNameFirst;
    }

    public void setDisplayNameFirst(String displayNameFirst) {
        this.displayNameFirst = displayNameFirst;
    }

    public String getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(String joinDate) {
        this.joinDate = joinDate;
    }

    public String getJoinDateForm() {
        return joinDateForm;
    }

    public void setJoinDateForm(String joinDateForm) {
        this.joinDateForm = joinDateForm;
    }

    public String getEditDate() {
        return editDate;
    }

    public void setEditDate(String editDate) {
        this.editDate = editDate;
    }

    public String getEditDateForm() {
        return editDateForm;
    }

    public void setEditDateForm(String editDateForm) {
        this.editDateForm = editDateForm;
    }

    public String getMyEmail() {
        return myEmail;
    }

    public void setMyEmail(String myEmail) {
        this.myEmail = myEmail;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public String getFacebookToken() {
        return facebookToken;
    }

    public void setFacebookToken(String facebookToken) {
        this.facebookToken = facebookToken;
    }

    public String getFacebookPhoto() {
        return facebookPhoto;
    }

    public void setFacebookPhoto(String facebookPhoto) {
        this.facebookPhoto = facebookPhoto;
    }

    public String getTwitterId() {
        return twitterId;
    }

    public void setTwitterId(String twitterId) {
        this.twitterId = twitterId;
    }

    public String getTwitterToken() {
        return twitterToken;
    }

    public void setTwitterToken(String twitterToken) {
        this.twitterToken = twitterToken;
    }

    public String getTwitterSecret() {
        return twitterSecret;
    }

    public void setTwitterSecret(String twitterSecret) {
        this.twitterSecret = twitterSecret;
    }

    public String getTwitterPhoto() {
        return twitterPhoto;
    }

    public void setTwitterPhoto(String twitterPhoto) {
        this.twitterPhoto = twitterPhoto;
    }

    public String getLinkedinId() {
        return linkedinId;
    }

    public void setLinkedinId(String linkedinId) {
        this.linkedinId = linkedinId;
    }

    public String getLinkedinToken() {
        return linkedinToken;
    }

    public void setLinkedinToken(String linkedinToken) {
        this.linkedinToken = linkedinToken;
    }

    public String getLinkedinSecret() {
        return linkedinSecret;
    }

    public void setLinkedinSecret(String linkedinSecret) {
        this.linkedinSecret = linkedinSecret;
    }

    public String getLinkedinPhoto() {
        return linkedinPhoto;
    }

    public void setLinkedinPhoto(String linkedinPhoto) {
        this.linkedinPhoto = linkedinPhoto;
    }

    public String getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(String userPhoto) {
        this.userPhoto = userPhoto;
    }

    public String getMyRating() {
        return myRating;
    }

    public void setMyRating(String myRating) {
        this.myRating = myRating;
    }

    public String getMyPhone() {
        return myPhone;
    }

    public void setMyPhone(String myPhone) {
        this.myPhone = myPhone;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getBioRaw() {
        return bioRaw;
    }

    public void setBioRaw(String bioRaw) {
        this.bioRaw = bioRaw;
    }

    public Integer getPro() {
        return pro;
    }

    public void setPro(Integer pro) {
        this.pro = pro;
    }

    public Integer getReviewsR() {
        return reviewsR;
    }

    public void setReviewsR(Integer reviewsR) {
        this.reviewsR = reviewsR;
    }

    public Integer getReviewsM() {
        return reviewsM;
    }

    public void setReviewsM(Integer reviewsM) {
        this.reviewsM = reviewsM;
    }

    public Integer getReviewsH() {
        return reviewsH;
    }

    public void setReviewsH(Integer reviewsH) {
        this.reviewsH = reviewsH;
    }

    public Integer getReviewsTotal() {
        return reviewsTotal;
    }

    public void setReviewsTotal(Integer reviewsTotal) {
        this.reviewsTotal = reviewsTotal;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public Integer getFilterAlcohol() {
        return filterAlcohol;
    }

    public void setFilterAlcohol(Integer filterAlcohol) {
        this.filterAlcohol = filterAlcohol;
    }

    public Integer getFilterHalal() {
        return filterHalal;
    }

    public void setFilterHalal(Integer filterHalal) {
        this.filterHalal = filterHalal;
    }

    public String getNumCoupMem() {
        return numCoupMem;
    }

    public void setNumCoupMem(String numCoupMem) {
        this.numCoupMem = numCoupMem;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getHomeAddressRaw() {
        return homeAddressRaw;
    }

    public void setHomeAddressRaw(String homeAddressRaw) {
        this.homeAddressRaw = homeAddressRaw;
    }

    public String getHomeCity() {
        return homeCity;
    }

    public void setHomeCity(String homeCity) {
        this.homeCity = homeCity;
    }

    public String getHomeCityRaw() {
        return homeCityRaw;
    }

    public void setHomeCityRaw(String homeCityRaw) {
        this.homeCityRaw = homeCityRaw;
    }

    public String getHomeState() {
        return homeState;
    }

    public void setHomeState(String homeState) {
        this.homeState = homeState;
    }

    public String getHomeStateRaw() {
        return homeStateRaw;
    }

    public void setHomeStateRaw(String homeStateRaw) {
        this.homeStateRaw = homeStateRaw;
    }

    public String getHomeZip() {
        return homeZip;
    }

    public void setHomeZip(String homeZip) {
        this.homeZip = homeZip;
    }

    public String getHomeLocation() {
        return homeLocation;
    }

    public void setHomeLocation(String homeLocation) {
        this.homeLocation = homeLocation;
    }

    public String getHomeCountry() {
        return homeCountry;
    }

    public void setHomeCountry(String homeCountry) {
        this.homeCountry = homeCountry;
    }

    public String getHomeCountryName() {
        return homeCountryName;
    }

    public void setHomeCountryName(String homeCountryName) {
        this.homeCountryName = homeCountryName;
    }

    public String getMyLat() {
        return myLat;
    }

    public void setMyLat(String myLat) {
        this.myLat = myLat;
    }

    public String getMyLon() {
        return myLon;
    }

    public void setMyLon(String myLon) {
        this.myLon = myLon;
    }

    public String getMyWeb() {
        return myWeb;
    }

    public void setMyWeb(String myWeb) {
        this.myWeb = myWeb;
    }

    public Integer getEmailAlerts() {
        return emailAlerts;
    }

    public void setEmailAlerts(Integer emailAlerts) {
        this.emailAlerts = emailAlerts;
    }

    public Integer getEmailOffers() {
        return emailOffers;
    }

    public void setEmailOffers(Integer emailOffers) {
        this.emailOffers = emailOffers;
    }

    public Integer getEmailRadius() {
        return emailRadius;
    }

    public void setEmailRadius(Integer emailRadius) {
        this.emailRadius = emailRadius;
    }

    public Integer getEmailMembers() {
        return emailMembers;
    }

    public void setEmailMembers(Integer emailMembers) {
        this.emailMembers = emailMembers;
    }

    public Integer getNumInv() {
        return numInv;
    }

    public void setNumInv(Integer numInv) {
        this.numInv = numInv;
    }

    public Integer getNumAdmin() {
        return numAdmin;
    }

    public void setNumAdmin(Integer numAdmin) {
        this.numAdmin = numAdmin;
    }

    public Integer getNumMail() {
        return numMail;
    }

    public void setNumMail(Integer numMail) {
        this.numMail = numMail;
    }

    public Integer getNumSent() {
        return numSent;
    }

    public void setNumSent(Integer numSent) {
        this.numSent = numSent;
    }

    public Integer getNumUnread() {
        return numUnread;
    }

    public void setNumUnread(Integer numUnread) {
        this.numUnread = numUnread;
    }

    public String getUrlPathMem() {
        return urlPathMem;
    }

    public void setUrlPathMem(String urlPathMem) {
        this.urlPathMem = urlPathMem;
    }

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("member_id")
    @Expose
    private Integer memberId;
    @SerializedName("mem_hash")
    @Expose
    private String memHash;
    @SerializedName("anon")
    @Expose
    private Integer anon;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("display_name")
    @Expose
    private String displayName;
    @SerializedName("display_name_first")
    @Expose
    private String displayNameFirst;
    @SerializedName("join_date")
    @Expose
    private String joinDate;
    @SerializedName("join_date_form")
    @Expose
    private String joinDateForm;
    @SerializedName("edit_date")
    @Expose
    private String editDate;
    @SerializedName("edit_date_form")
    @Expose
    private String editDateForm;
    @SerializedName("my_email")
    @Expose
    private String myEmail;
    @SerializedName("facebook_id")
    @Expose
    private String facebookId;
    @SerializedName("facebook_token")
    @Expose
    private String facebookToken;
    @SerializedName("facebook_photo")
    @Expose
    private String facebookPhoto;
    @SerializedName("twitter_id")
    @Expose
    private String twitterId;
    @SerializedName("twitter_token")
    @Expose
    private String twitterToken;
    @SerializedName("twitter_secret")
    @Expose
    private String twitterSecret;
    @SerializedName("twitter_photo")
    @Expose
    private String twitterPhoto;
    @SerializedName("linkedin_id")
    @Expose
    private String linkedinId;
    @SerializedName("linkedin_token")
    @Expose
    private String linkedinToken;
    @SerializedName("linkedin_secret")
    @Expose
    private String linkedinSecret;
    @SerializedName("linkedin_photo")
    @Expose
    private String linkedinPhoto;
    @SerializedName("user_photo")
    @Expose
    private String userPhoto;
    @SerializedName("my_rating")
    @Expose
    private String myRating;
    @SerializedName("my_phone")
    @Expose
    private String myPhone;
    @SerializedName("lang")
    @Expose
    private String lang;
    @SerializedName("bio")
    @Expose
    private String bio;
    @SerializedName("bio_raw")
    @Expose
    private String bioRaw;
    @SerializedName("pro")
    @Expose
    private Integer pro;
    @SerializedName("reviews_r")
    @Expose
    private Integer reviewsR;
    @SerializedName("reviews_m")
    @Expose
    private Integer reviewsM;
    @SerializedName("reviews_h")
    @Expose
    private Integer reviewsH;
    @SerializedName("reviews_total")
    @Expose
    private Integer reviewsTotal;
    @SerializedName("authority")
    @Expose
    private String authority;
    @SerializedName("filter_alcohol")
    @Expose
    private Integer filterAlcohol;
    @SerializedName("filter_halal")
    @Expose
    private Integer filterHalal;
    @SerializedName("num_coup_mem")
    @Expose
    private String numCoupMem;
    @SerializedName("home_address")
    @Expose
    private String homeAddress;
    @SerializedName("home_address_raw")
    @Expose
    private String homeAddressRaw;
    @SerializedName("home_city")
    @Expose
    private String homeCity;
    @SerializedName("home_city_raw")
    @Expose
    private String homeCityRaw;
    @SerializedName("home_state")
    @Expose
    private String homeState;
    @SerializedName("home_state_raw")
    @Expose
    private String homeStateRaw;
    @SerializedName("home_zip")
    @Expose
    private String homeZip;
    @SerializedName("home_location")
    @Expose
    private String homeLocation;
    @SerializedName("home_country")
    @Expose
    private String homeCountry;
    @SerializedName("home_country_name")
    @Expose
    private String homeCountryName;
    @SerializedName("my_lat")
    @Expose
    private String myLat;
    @SerializedName("my_lon")
    @Expose
    private String myLon;
    @SerializedName("my_web")
    @Expose
    private String myWeb;
    @SerializedName("email_alerts")
    @Expose
    private Integer emailAlerts;
    @SerializedName("email_offers")
    @Expose
    private Integer emailOffers;
    @SerializedName("email_radius")
    @Expose
    private Integer emailRadius;
    @SerializedName("email_members")
    @Expose
    private Integer emailMembers;
    @SerializedName("num_inv")
    @Expose
    private Integer numInv;
    @SerializedName("num_admin")
    @Expose
    private Integer numAdmin;
    @SerializedName("num_mail")
    @Expose
    private Integer numMail;
    @SerializedName("num_sent")
    @Expose
    private Integer numSent;
    @SerializedName("num_unread")
    @Expose
    private Integer numUnread;
    @SerializedName("url_path_mem")
    @Expose
    private String urlPathMem;
    @Transient
    @Exclude
    //@SerializedName("status_text")
    //@Expose(serialize = false, deserialize = false)
    @Nullable
    private transient JsonElement status_textBackup = null;
    @SerializedName("status_text")
    @Expose
    private String status_text;
    private final static long serialVersionUID = 4412182952467891727L;

    /**
     * No args constructor for use in serialization
     */
    public Member() {
    }
}
