package com.zabihah.ui.data.interfaces;

import java.util.Map;

public class MinbarInterfaces {
    public interface SocialAuthenticationListener{
        void onUserAuthenticated(Map<String, String> accountMap, Map<String, String> profileData);
    }
}
