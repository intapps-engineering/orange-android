package com.zabihah.ui.data.api;

import com.zabihah.ui.ZabihahApplication;
import com.zabihah.ui.data.api.responses.AuthenticationResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

public class HttpService {
    private static final String TAG = "HttpService";

    public HttpService() {

    }

    private static ApiService getApiService(Map<String, String> headers) {
        if (apiService == null || (headers == null || headers.isEmpty())) {
            apiService = ApiModule.getApiModuleInstance(ZabihahApplication.getCacheFile(), headers).getApiService();
        }

        return apiService;
    }

    private static ApiService apiService = null;


    public static void resetApiService() {
        apiService = null;
    }

    public void authenticateUser(Map<String, String> requestMap, Callback<AuthenticationResponse> callback) {
        Call<AuthenticationResponse> call = getApiService(null).authenticateUser(requestMap);
        call.enqueue(callback);
    }
}
