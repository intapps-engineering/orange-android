package com.zabihah.ui.data;

public enum ZabihahPage {
    HOME("home", "home-page.html"),
    GUEST("guest", "home-page.html"),
    LOGIN("login", "login.html"),
    EDIT_PROFILE("edit-profile", "create-edit-profile.html"),
    SAVED_PLACES("saved-places", "profile-saved-places.html"),
    ADD_RECORD("add-record", "add"),
    PRIVACY_POLICY("privacy-policy", "com/privacy"),
    TERMS("terms", "com/tos"),
    CONTACT("contact", "mob/com/contact"),
    ABOUT("about", "mob/com/about"),
    HELP("help", "mob/com/faq"),
    LEADERBOARD("leaderboard", "login.html");

    private String type;
    private String url;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    private ZabihahPage(String type, String url) {
        this.type = type;
        this.url = url;
    }

    @Override
    public String toString() {
        return "Page{" +
                "type='" + type + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
