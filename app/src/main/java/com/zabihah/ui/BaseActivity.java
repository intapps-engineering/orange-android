package com.zabihah.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.Gson;
import com.tbruyelle.rxpermissions3.RxPermissions;
import com.zabihah.ui.data.Constants;
import com.zabihah.ui.data.PermissionRequestListener;
import com.zabihah.ui.data.SharedPrefsUtils;
import com.zabihah.ui.data.ZabihahPage;
import com.zabihah.ui.data.api.HttpService;
import com.zabihah.ui.data.api.responses.Member;

public class BaseActivity extends AppCompatActivity {
    private HttpService httpService;
    public static final int LOCATION_PERMISSION_REQUEST_CODE = 1000;

    private AlertDialog GPSPromptAlertDialog;
    private AlertDialog zabihahPermissionRequestDialog;
    private AlertDialog locationPermissionRationaleDialog;
    private AlertDialog locationPermanentDenialPrompt;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private LocationRequest locationRequest;
    private boolean isLocationPromptShownForSession;

    public HttpService getHttpService() {
        if (httpService == null) {
            httpService = new HttpService();
        }

        return httpService;
    }

    public void openMainActivity(Activity activity, ZabihahPage zabihahPage) {
        Intent intent = new Intent(activity, MainActivity.class);
        intent.putExtra(Constants.page, zabihahPage);
        SharedPrefsUtils.setStringPreference(activity, Constants.page, zabihahPage.getType());
        startActivity(intent);
        finish();
    }

    public void openAuthActivity(Activity activity) {
        SharedPrefsUtils.setBooleanPreference(activity, Constants.isUserSignedIn, false);
        FirebaseAuth.getInstance().signOut();
        Intent intent = new Intent(activity, AuthActivity.class);
        startActivity(intent);
        finish();
    }

    public void showLocationPermissionRequestPrompt(Activity activity, LocationRequestListener listener) {
        if(ZabihahApplication.hasLocationPromptBeenShownForSession()){
            return;
        }
        ZabihahApplication.setLocationPromptFlag(true);
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(activity, R.style.AlertDialogTheme);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(getString(R.string.zabihah_location_prompt_reason));
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.label_proceed, (dialogInterface, i) -> {
            if(listener != null){
                listener.onProceedClicked();
            }
            if (dialogInterface != null) {
                dialogInterface.dismiss();
            }
        });
        builder.setNegativeButton(R.string.cancel, (dialogInterface, which) -> {
            if(listener != null){
                listener.onCancelClicked();
            }
            if (dialogInterface != null) {
                dialogInterface.dismiss();
            }
        });
        if (zabihahPermissionRequestDialog == null) {
            zabihahPermissionRequestDialog = builder.create();
        }
        if (!zabihahPermissionRequestDialog.isShowing() && !isFinishing()) {
            zabihahPermissionRequestDialog.show();
        }
    }

    public interface LocationRequestListener {
        public void onProceedClicked();
        public void onCancelClicked();
    }


    public boolean isGPSEnabled(Activity activity) {
        LocationManager locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);

        if (locationManager != null && locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            return true;
        }

        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(activity, R.style.AlertDialogTheme);
        builder.setMessage(getString(R.string.location_services_prompt));
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.label_proceed, (dialogInterface, i) -> {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            activity.startActivity(intent);
        });
        builder.setNegativeButton(R.string.cancel, (dialog, which) -> {
            if (dialog != null) {
                dialog.dismiss();
            }
        });
        if (GPSPromptAlertDialog == null) {
            GPSPromptAlertDialog = builder.create();
        }
        if (!GPSPromptAlertDialog.isShowing() && !isFinishing()) {
            GPSPromptAlertDialog.show();
        }
        //GPSPromptAlertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.RED);
        //GPSPromptAlertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.BLACK);

        return false;
    }

    @SuppressLint("CheckResult")
    public void checkLocationPermission(final Activity activity, final PermissionRequestListener listener) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                /*if (!ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_COARSE_LOCATION) || !ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_FINE_LOCATION)) {
                    showPermanentPermissionDenialPrompt(activity);
                    return;
                }*/

                final RxPermissions rxPermissions = new RxPermissions(this);
                rxPermissions
                        .requestEachCombined(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
                        .subscribe(permission -> {
                            if (permission.granted) {
                                if (listener != null) {
                                    listener.onPermissionGranted();
                                }
                            } else if (permission.shouldShowRequestPermissionRationale) {
                                showPermissionRationaleDialog(activity);
                            } else {
                                showPermanentPermissionDenialPrompt(activity);
                            }
                        });
            } else {
                cancelDialog(locationPermissionRationaleDialog);
                cancelDialog(locationPermanentDenialPrompt);
                if (listener != null) {
                    listener.onPermissionGranted();
                }
            }
        } else {
            if (listener != null) {
                listener.onPermissionGranted();
            }
        }
    }

    public void showPermissionRationaleDialog(Activity activity) {
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(activity, R.style.AlertDialogTheme);
        builder.setTitle(getString(R.string.permission_request_dialog_title));
        builder.setMessage(getString(R.string.location_permission_rationale));
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.label_proceed, (dialogInterface, i) -> {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_REQUEST_CODE);
            //requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_REQUEST_CODE);
        });
        builder.setNegativeButton(R.string.cancel, (dialog, which) -> {
            if (dialog != null) {
                dialog.dismiss();
            }
            cancelDialog(locationPermissionRationaleDialog);
            cancelDialog(locationPermanentDenialPrompt);
        });

        if (locationPermissionRationaleDialog == null) {
            locationPermissionRationaleDialog = builder.create();
        }

        //locationPermissionRationaleDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.RED);
        //locationPermissionRationaleDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.BLACK);

        if (!locationPermissionRationaleDialog.isShowing()) {
            locationPermissionRationaleDialog.show();
        }
    }

    public void showPermanentPermissionDenialPrompt(Activity activity) {
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(activity, R.style.AlertDialogTheme);
        builder.setTitle(getString(R.string.permission_request_dialog_title));
        builder.setMessage(getString(R.string.location_manual_permission_rationale));
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.label_proceed, (dialogInterface, i) -> {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
            intent.setData(uri);
            activity.startActivity(intent);
        });
        builder.setNegativeButton(R.string.cancel, (dialog, which) -> {
            if (dialog != null) {
                dialog.dismiss();
            }
            cancelDialog(locationPermissionRationaleDialog);
            cancelDialog(locationPermanentDenialPrompt);
        });

        if (locationPermanentDenialPrompt == null) {
            locationPermanentDenialPrompt = builder.create();
        }
        if (!locationPermanentDenialPrompt.isShowing()) {
            locationPermanentDenialPrompt.show();
        }
    }

    private void cancelDialog(AlertDialog alertDialog) {
        if (alertDialog != null) {
            alertDialog.dismiss();
        }
    }

    public void initLocationListener(Activity activity) {
        isLocationPromptShownForSession = true;
        showLocationPermissionRequestPrompt(this, new LocationRequestListener() {
            @Override
            public void onProceedClicked() {
                if (isGPSEnabled(activity)) {
                    checkLocationPermission(activity, () -> {
                        if (fusedLocationProviderClient == null) {
                            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(activity);
                            // fusedLocationProviderClient.getLastLocation().addOnSuccessListener(this, this::persistLocationData);
                        }

                        if (locationRequest == null) {
                            locationRequest = LocationRequest.create().setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY).setInterval(10000).setSmallestDisplacement(2000);
                        }

                        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper());
                    });
                }
            }

            @Override
            public void onCancelClicked() {

            }
        });
    }

    private final LocationCallback locationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            if (locationResult == null) {
                return;
            }
            persistLocationData(locationResult.getLastLocation());
        }
    };

    public void persistUserData(Member member) {
        if (member != null) {
            Gson gson = new Gson();
            String memberDataAsString = gson.toJson(member);
            SharedPrefsUtils.setStringPreference(this, Constants.member, memberDataAsString);

            SharedPrefsUtils.setStringPreference(this, Constants.memberFirstName, member.getFirstName());
            SharedPrefsUtils.setStringPreference(this, Constants.memberLastName, member.getLastName());
            SharedPrefsUtils.setStringPreference(this, Constants.memberAddress, member.getHomeAddress());
            SharedPrefsUtils.setStringPreference(this, Constants.memberPhoto, member.getUserPhoto());

        }
    }

    public void persistLocationData(Location location) {
        if (location != null) {
            Log.w("Location Updated", location.toString());
            SharedPrefsUtils.setStringPreference(this, Constants.latitude, String.valueOf(location.getLatitude()));
            SharedPrefsUtils.setStringPreference(this, Constants.longitude, String.valueOf(location.getLongitude()));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cancelDialog(locationPermissionRationaleDialog);
        cancelDialog(locationPermanentDenialPrompt);
        cancelDialog(GPSPromptAlertDialog);
        if (fusedLocationProviderClient != null) {
            fusedLocationProviderClient.removeLocationUpdates(locationCallback);
        }
    }
}
