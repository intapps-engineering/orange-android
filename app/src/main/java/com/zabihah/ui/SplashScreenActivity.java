package com.zabihah.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.zabihah.ui.data.Constants;
import com.zabihah.ui.data.SharedPrefsUtils;

public class SplashScreenActivity extends AppCompatActivity {
    private static final long SPLASH_DURATION = 3000;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);
        delayScreen();
    }

    private void delayScreen() {
        Handler splashScreenHandler = new Handler();
        splashScreenHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
//                startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
                boolean isUserSignedIn = SharedPrefsUtils.getBooleanPreference(SplashScreenActivity.this, Constants.isUserSignedIn, false);
                startActivity(new Intent(SplashScreenActivity.this, isUserSignedIn ? MainActivity.class: AuthActivity.class));
                finish();
            }
        }, SPLASH_DURATION);
    }


}
