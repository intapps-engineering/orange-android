package com.zabihah.ui;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.net.http.SslError;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.webkit.ConsoleMessage;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.zabihah.ui.data.Constants;
import com.zabihah.ui.data.SharedPrefsUtils;
import com.zabihah.ui.data.ZabihahPage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends BaseActivity implements View.OnClickListener {
    private AppBarConfiguration mAppBarConfiguration;
    private AppCompatTextView homeTextView, editProfileTextView, viewSavedPlacesTextView, addRecordTextView,
            privacyPolicyTextView, termsOfServiceTextView, aboutZabihahTextView, helpFaqTextView, contactUsTextView,
            leaderboardTextView, signoutTextView, userFullName, userLocation;
    private DrawerLayout drawer;
    private LinearLayout homeLinearLayout, editProfileLinearLayout, viewSavedPlacesLinearLayout,
            addRecordLinearLayout;
    private Toolbar toolbar;
    private AppCompatImageView sideMenuIcon, verifiedImageView;
    private CircleImageView profilePhoto;
    private static final String LOGIN_WEB_PAGE_LOCAL = "https://192.168.1.104"; //"http://10.0.2.2"; //"https://localhost"; //https://halalapalooza.com/app/"; //
    private static final String WEB_DOMAIN = "https://halalapalooza.com/app/";
    private static final String ZABIHAH_WEB_DOMAIN = "https://zabihah.com/";
    private static WebView loginWebView;
    private WebSettings webSettings;
    private static ZabihahPage zabihahPage = ZabihahPage.LOGIN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);

        if (getIntent() != null && getIntent().hasExtra(Constants.page)) {
            zabihahPage = (ZabihahPage) getIntent().getSerializableExtra(Constants.page);
        } else {
            zabihahPage = ZabihahPage.valueOf(SharedPrefsUtils.getStringPreference(this, Constants.page).toUpperCase());
        }

        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initLocationListener(this);
    }

    private void initView() {
        homeTextView = findViewById(R.id.nav_home);
        editProfileTextView = findViewById(R.id.nav_edit_user_profile);
        viewSavedPlacesTextView = findViewById(R.id.nav_user_saved_places);
        addRecordTextView = findViewById(R.id.nav_add_record);
        privacyPolicyTextView = findViewById(R.id.nav_privacy_policy);
        termsOfServiceTextView = findViewById(R.id.nav_terms_of_service);
        aboutZabihahTextView = findViewById(R.id.nav_about_zabihah);
        helpFaqTextView = findViewById(R.id.nav_help_faq);
        contactUsTextView = findViewById(R.id.nav_contact_us);
        leaderboardTextView = findViewById(R.id.nav_leaderboard);
        signoutTextView = findViewById(R.id.nav_signout);
        homeLinearLayout = findViewById(R.id.nav_home_ll);
        editProfileLinearLayout = findViewById(R.id.nav_edit_user_profile_ll);
        viewSavedPlacesLinearLayout = findViewById(R.id.nav_user_saved_places_ll);
        addRecordLinearLayout = findViewById(R.id.nav_add_record_ll);
        sideMenuIcon = findViewById(R.id.side_bar_menu);
        verifiedImageView = findViewById(R.id.verified_image_view);
        userFullName = findViewById(R.id.nav_header_user_name);
        userLocation = findViewById(R.id.nav_header_user_location);
        profilePhoto = findViewById(R.id.profile_photo);

        privacyPolicyTextView.setOnClickListener(this);
        termsOfServiceTextView.setOnClickListener(this);
        aboutZabihahTextView.setOnClickListener(this);
        helpFaqTextView.setOnClickListener(this);
        contactUsTextView.setOnClickListener(this);
        leaderboardTextView.setOnClickListener(this);
        signoutTextView.setOnClickListener(this);
        sideMenuIcon.setOnClickListener(v -> drawer.openDrawer(Gravity.LEFT));
        homeLinearLayout.setOnClickListener(this);
        editProfileLinearLayout.setOnClickListener(this);
        viewSavedPlacesLinearLayout.setOnClickListener(this);
        addRecordLinearLayout.setOnClickListener(this);

        loginWebView = findViewById(R.id.home_webview);
        initializeWebView();

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                setSideMenuValues();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);

            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();
    }

    private void initializeWebView() {
        webSettings = loginWebView.getSettings();
        loginWebView.getSettings().setLoadsImagesAutomatically(true);
        loginWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        loginWebView.setWebViewClient(new WebViewClient() {
            /*
            @Override
             public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                 switch (error.getPrimaryError()) {
                     case SslError.SSL_UNTRUSTED:
                         Log.e("x", "SslError : The certificate authority is not trusted.");
                         break;
                     case SslError.SSL_EXPIRED:
                         Log.e("x", "SslError : The certificate has expired.");
                         break;
                     case SslError.SSL_IDMISMATCH:
                         Log.e("x", "The certificate Hostname mismatch.");
                         break;
                     case SslError.SSL_NOTYETVALID:
                         Log.e("x", "The certificate is not yet valid.");
                         break;
                 }
                 handler.proceed();
             }
            */

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return super.shouldOverrideUrlLoading(view, request);
            }
        });
        loginWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                Log.w("MyApplication", consoleMessage.message() + " -- From line " +
                        consoleMessage.lineNumber() + " of " + consoleMessage.sourceId());
                return true;
            }
        });
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setAppCacheEnabled(false);
        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        loginWebView.addJavascriptInterface(new WebInterface(this), "Android");

        Map<String, String> noCacheHeaders = new HashMap<String, String>(2);
        noCacheHeaders.put("Pragma", "no-cache");
        noCacheHeaders.put("Cache-Control", "no-cache");

        loginWebView.loadUrl(String.format("%s%s", WEB_DOMAIN, zabihahPage.getUrl()), noCacheHeaders);

    }

    private void loadPage(ZabihahPage page) {
        webSettings = loginWebView.getSettings();
        loginWebView.getSettings().setLoadsImagesAutomatically(true);
        loginWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        loginWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return super.shouldOverrideUrlLoading(view, request);
            }
        });
        loginWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                Log.w("MyApplication", consoleMessage.message() + " -- From line " +
                        consoleMessage.lineNumber() + " of " + consoleMessage.sourceId());
                return true;
            }
        });
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setAppCacheEnabled(false);
        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        loginWebView.addJavascriptInterface(new WebInterface(this), "Android");

        Map<String, String> noCacheHeaders = new HashMap<String, String>(2);
        noCacheHeaders.put("Pragma", "no-cache");
        noCacheHeaders.put("Cache-Control", "no-cache");

        if (page.getType().equals(ZabihahPage.EDIT_PROFILE.getType()) || page.getType().equals(ZabihahPage.SAVED_PLACES.getType())
                || page.getType().equals(ZabihahPage.HOME.getType())) {
            loginWebView.loadUrl(String.format("%s%s", WEB_DOMAIN, page.getUrl()), noCacheHeaders);
        } else {
            loginWebView.loadUrl(String.format("%s%s", ZABIHAH_WEB_DOMAIN, page.getUrl()), noCacheHeaders);
        }

    }

    public static void storeUserData(Context mContext) {
        loginWebView.post(() -> {
            if (zabihahPage != null && zabihahPage.getType().equalsIgnoreCase(ZabihahPage.HOME.getType())) {
                SharedPrefsUtils.setBooleanPreference(mContext, Constants.isUserSignedIn, true);
                String memberDataAsString = SharedPrefsUtils.getStringPreference(mContext, Constants.member);
                Log.e("XX", memberDataAsString);
                loginWebView.loadUrl("javascript:storeUserData('" + memberDataAsString + "')");
            } else {
                SharedPrefsUtils.setBooleanPreference(mContext, Constants.isUserSignedIn, false);
                loginWebView.loadUrl("javascript:storeUserData('" + "" + "')");
            }
            loginWebView.loadUrl("javascript:initActions()");
        });
    }

    public static void storeLocationData(Context context) {
        loginWebView.post(() -> {
            String latitude = SharedPrefsUtils.getStringPreference(context, Constants.latitude);
            String longitude = SharedPrefsUtils.getStringPreference(context, Constants.longitude);
            String formattedLocationData = latitude + "," + longitude;
            Log.w("Upload Location Data", formattedLocationData);
            loginWebView.loadUrl("javascript:storeLocationData('" + formattedLocationData + "')");
        });
    }

    private void setSideMenuValues() {
        String firstName = SharedPrefsUtils.getStringPreference(this, Constants.memberFirstName);
        String lastName = SharedPrefsUtils.getStringPreference(this, Constants.memberLastName);
        String address = SharedPrefsUtils.getStringPreference(this, Constants.memberAddress);
        String photoUrl = SharedPrefsUtils.getStringPreference(this, Constants.memberPhoto);


        new DownloadImageTask(profilePhoto).execute(photoUrl);
        userFullName.setText(firstName + " " + lastName);
        userLocation.setText(address);
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    if (loginWebView.canGoBack()) {
                        loginWebView.goBack();
                    } else {
                        finish();
                    }
                    return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideSystemUI();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void hideSystemUI() {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    private void logOut() {
        // TODO
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public void onClick(View v) {
        closeSideMenu();
        switch (v.getId()) {
            case (R.id.nav_home_ll):
                zabihahPage = ZabihahPage.HOME;
                storeUserData(this);
                storeLocationData(this);
                loadPage(ZabihahPage.HOME);
                //loginWebView.loadUrl("javascript:storeLocationData('34.23,23.21')");
                break;
            case (R.id.nav_edit_user_profile_ll):
                loadPage(ZabihahPage.EDIT_PROFILE);
                break;
            case (R.id.nav_contact_us):
                loadPage(ZabihahPage.CONTACT);
                break;
            case (R.id.nav_user_saved_places_ll):
                loadPage(ZabihahPage.SAVED_PLACES);
                break;
            case (R.id.nav_add_record_ll):
                loadPage(ZabihahPage.ADD_RECORD);
                break;
            case (R.id.nav_privacy_policy):
                loadPage(ZabihahPage.PRIVACY_POLICY);
                break;
            case (R.id.nav_about_zabihah):
                loadPage(ZabihahPage.ABOUT);
                break;
            case (R.id.nav_terms_of_service):
                loadPage(ZabihahPage.TERMS);
                break;
            case (R.id.nav_help_faq):
                loadPage(ZabihahPage.HELP);
                break;
            case (R.id.nav_leaderboard):
                loadPage(ZabihahPage.LEADERBOARD);
                break;
            case (R.id.nav_signout):
                openAuthActivity(this);
                break;
        }
    }

    public void closeSideMenu() {
        drawer.closeDrawer(GravityCompat.START);
    }

    private static void uploadCachedLocationData(Context context) {
        String latitude = SharedPrefsUtils.getStringPreference(context, Constants.latitude);
        String longitude = SharedPrefsUtils.getStringPreference(context, Constants.longitude);
        if (loginWebView != null) {
            String formattedLocationData = latitude + "," + longitude;
            Log.w("Location Data", formattedLocationData);
            loginWebView.loadUrl("javascript:storeLocationData('" + formattedLocationData + "')");
        }
    }
}