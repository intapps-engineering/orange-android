package com.zabihah.ui;

import android.content.res.Resources;

import androidx.multidex.MultiDexApplication;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

import java.io.File;


public class ZabihahApplication extends MultiDexApplication {
    static File cacheFile;
    private static ZabihahApplication zabihahApplication;
    private static boolean locationPromptFlag;

    @Override
    public void onCreate() {
        super.onCreate();
        cacheFile = new File(getCacheDir(), "responses");
        zabihahApplication = this;
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
    }

    public static File getCacheFile() {
        return cacheFile;
    }

    public static boolean hasLocationPromptBeenShownForSession() {
        return locationPromptFlag;
    }

    public static void setLocationPromptFlag(boolean locationPromptFlag) {
        ZabihahApplication.locationPromptFlag = locationPromptFlag;
    }

    public static ZabihahApplication getAppInstance() {
        return zabihahApplication;
    }

    public static Resources getApplicationResources() {
        return getAppInstance().getResources();
    }
}
