package com.zabihah.ui;

import android.content.Context;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import com.zabihah.ui.data.Constants;
import com.zabihah.ui.data.SharedPrefsUtils;

import org.json.JSONException;
import org.json.JSONObject;

public class WebInterface {
    Context mContext;

    /**
     * Instantiate the interface and set the context
     */
    WebInterface(Context c) {
        mContext = c;
    }

    /**
     * Show a toast from the web page
     */
    @JavascriptInterface
    public void showToast(String toast) {
        //Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
        Log.w("Zabihah WebInterface", toast);
        if (toast.contains("username")) {
            Log.e("MF", toast);
            toast = toast.replace("Stored ", "");
            if (toast != null) {
                try {
                    String formatted = toast.replaceAll("\n", "\\n");
                    JSONObject member = new JSONObject(formatted);
                    SharedPrefsUtils.setStringPreference(mContext, Constants.member, toast);
                    SharedPrefsUtils.setStringPreference(mContext, Constants.latitude, member.get("my_lat").toString());
                    SharedPrefsUtils.setStringPreference(mContext, Constants.longitude, member.get("my_lon").toString());
                    SharedPrefsUtils.setStringPreference(mContext, Constants.memberFirstName, member.get("first_name").toString());
                    SharedPrefsUtils.setStringPreference(mContext, Constants.memberLastName, member.get("last_name").toString());
                    SharedPrefsUtils.setStringPreference(mContext, Constants.memberAddress, member.get("home_city").toString() + ", " + member.get("home_state").toString());
                    SharedPrefsUtils.setStringPreference(mContext, Constants.memberPhoto, member.get("user_photo").toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @JavascriptInterface
    public void homePageDoneLoading() {
        Toast.makeText(mContext, "Home page loaded", Toast.LENGTH_SHORT).show();
        MainActivity.storeUserData(mContext);
        MainActivity.storeLocationData(mContext);
    }
}
