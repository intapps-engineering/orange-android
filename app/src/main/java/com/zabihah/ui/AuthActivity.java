package com.zabihah.ui;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.AppCompatButton;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.LoginStatusCallback;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.OAuthProvider;
import com.zabihah.ui.data.ZabihahPage;
import com.zabihah.ui.data.api.responses.AuthenticationResponse;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AuthActivity extends BaseActivity implements View.OnClickListener {
    private AppBarConfiguration mAppBarConfiguration;
    private CallbackManager callbackManager;
    private ProfileTracker profileTracker;
    private Profile profile;
    private AccessToken accessToken;
    private static final String TAG = "Zabihah Firebase Auth";
    private ProgressBar progressBar;
    private boolean isLocationPromptShownForSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createKeyHash(this, "com.zabihah.ui");
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_auth_screen);
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isLocationPromptShownForSession) {
            initLocationListener(this);
            isLocationPromptShownForSession = true;
        }
    }

    private void initView() {
        AppCompatButton loginWithEmailButton = findViewById(R.id.email_login_button);
        AppCompatButton loginWithFacebookButton = findViewById(R.id.facebook_login_button);
        AppCompatButton loginWithAppleButton = findViewById(R.id.apple_login_button);
        AppCompatButton loginAsGuestButton = findViewById(R.id.guest_login_button);
        progressBar = findViewById(R.id.progress_bar);

        loginWithEmailButton.setOnClickListener(v -> {
            openMainActivity(AuthActivity.this, ZabihahPage.LOGIN);
        });

        loginWithFacebookButton.setOnClickListener(v -> {
            initializeFacebookAuthentication();
        });

        loginWithAppleButton.setOnClickListener(v -> {
            initializeAppleAuthentication();
        });

        loginAsGuestButton.setOnClickListener(v -> {
            openMainActivity(AuthActivity.this, ZabihahPage.GUEST);
        });
    }

    private void performFacebookAuthentication() {
        boolean isLoggedIn = accessToken != null && !accessToken.isExpired();
        if (!isLoggedIn) {
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
        } else {
            getUserDataFromFacebook(accessToken, profile);
        }
    }

    private void initializeFacebookAuthentication() {
        accessToken = AccessToken.getCurrentAccessToken();
        profile = Profile.getCurrentProfile();

        callbackManager = CallbackManager.Factory.create();

        if (accessToken != null && !accessToken.isExpired() && !accessToken.isDataAccessExpired()) {
            retrieveFacebookLoginSession();
        } else {
            LoginManager.getInstance().registerCallback(callbackManager, loginResult);
            performFacebookAuthentication();
        }

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(
                    Profile oldProfile,
                    Profile currentProfile) {
            }
        };
    }

    private void retrieveFacebookLoginSession() {
        LoginManager.getInstance().retrieveLoginStatus(this, new LoginStatusCallback() {
            @Override
            public void onCompleted(AccessToken accessTokenParam) {
                accessToken = accessTokenParam;
                getUserDataFromFacebook(accessToken, profile);
            }

            @Override
            public void onFailure() {
                Toast.makeText(AuthActivity.this, "There was an issue completing your Facebook authentication, please try again", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(Exception exception) {
                exception.printStackTrace();
                Toast.makeText(AuthActivity.this, "Facebook Authentication: " + exception.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initializeAppleAuthentication() {
        OAuthProvider.Builder provider = OAuthProvider.newBuilder("apple.com");
        List<String> scopes =
                new ArrayList<String>() {
                    {
                        add("email");
                        add("name");
                    }
                };
        provider.setScopes(scopes);
        provider.addCustomParameter("locale", "en");
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();

        if (firebaseUser != null) {
            firebaseUser
                    .startActivityForReauthenticateWithProvider(/* activity= */ this, provider.build())
                    .addOnSuccessListener(firebaseAuthSuccessListener)
                    .addOnFailureListener(firebaseAuthFailureListener);
        } else {
            Task<AuthResult> pending = firebaseAuth.getPendingAuthResult();
            if (pending != null) {
                pending.addOnSuccessListener(firebaseAuthSuccessListener)
                        .addOnFailureListener(firebaseAuthFailureListener);
            } else {
                firebaseAuth.startActivityForSignInWithProvider(this, provider.build())
                        .addOnSuccessListener(firebaseAuthSuccessListener)
                        .addOnFailureListener(firebaseAuthFailureListener);
            }
        }
    }

    private void getUserDataFromFacebook(AccessToken accessToken, Profile profile) {
        GraphRequest request = GraphRequest.newMeRequest(
                accessToken,
                (object, response) -> {
                    String emailAddress = "";
                    try {
                        emailAddress = object.getString("email");
                        String fullName = object.getString("name");
                        String id = object.getString("id");
                        String firstName = "";
                        String lastName = "";

                        if (profile != null) {
                            firstName = profile.getFirstName();
                            lastName = profile.getLastName();
                        }

                        if (TextUtils.isEmpty(firstName) && TextUtils.isEmpty(lastName)) {
                            if (!TextUtils.isEmpty(fullName) && fullName.contains(" ")) {
                                String[] splitNames = fullName.split(" ");
                                firstName = splitNames[0];
                                lastName = splitNames[1];
                            } else {
                                firstName = fullName;
                            }
                        }
                        loginToServer(firstName, lastName, emailAddress, id, null);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link,email");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void loginToServer(String firstName, String lastName, String emailAddress, String facebookId, String appleId) {
        progressBar.setVisibility(View.VISIBLE);
        progressBar.setIndeterminate(true);
        Map<String, String> queryMap = new HashMap<>();
        queryMap.put("key", BuildConfig.ZabihahKey);
        queryMap.put("uuid", UUID.randomUUID().toString());
        queryMap.put("first_name", firstName);
        queryMap.put("last_name", lastName);
        queryMap.put("username", emailAddress);
        queryMap.put("email", emailAddress);
        queryMap.put("terms", "1");
        if (!TextUtils.isEmpty(facebookId)) {
            queryMap.put("fbid", facebookId);
        } else if (!TextUtils.isEmpty(appleId)) {
            queryMap.put("apple_id", appleId);
        } else {
            queryMap.put("fbid", "");
            queryMap.put("apple_id", "");
        }
        String url = "https://app.zabihah.com/mem_register.php?key=${key}&uuid=${uuid}&first_name=${firstName}&last_name=${lastName}&username=${emailAddress}&email=${emailAddress}&terms=1&fbid=${facebookId}";

        getHttpService().authenticateUser(queryMap, new Callback<AuthenticationResponse>() {
            @Override
            public void onResponse(Call<AuthenticationResponse> call, Response<AuthenticationResponse> response) {
                AuthenticationResponse authenticationResponse = response.body();
                if (authenticationResponse != null && authenticationResponse.getMember() != null) {
                    progressBar.setVisibility(View.GONE);
                    persistUserData(authenticationResponse.getMember());
                    openMainActivity(AuthActivity.this, ZabihahPage.HOME);
                } else {
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<AuthenticationResponse> call, Throwable t) {
                t.printStackTrace();
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (callbackManager != null) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    finish();
                    return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideSystemUI();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void hideSystemUI() {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (profileTracker != null) {
            profileTracker.stopTracking();
        }
    }

    private final FacebookCallback<LoginResult> loginResult = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            Toast.makeText(AuthActivity.this, "Login Successful: " + loginResult.getAccessToken().getToken(), Toast.LENGTH_SHORT).show();
            getUserDataFromFacebook(loginResult.getAccessToken(), profile);
        }

        @Override
        public void onCancel() {
            Toast.makeText(AuthActivity.this, "There was an issue completing your Facebook authentication, please try again", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onError(FacebookException exception) {
            exception.printStackTrace();
            Toast.makeText(AuthActivity.this, "Sign in with Facebook: " + exception.getMessage(), Toast.LENGTH_SHORT).show();
        }
    };

    private final OnSuccessListener<AuthResult> firebaseAuthSuccessListener = authResult -> {
        FirebaseUser user = authResult.getUser();
        if (user != null) {
            String fullName = user.getDisplayName();
            String id = user.getUid();
            String firstName = "";
            String lastName = "";
            String emailAddress = user.getEmail();
            if (!TextUtils.isEmpty(fullName) && fullName != null && !fullName.isEmpty() && fullName.contains(" ")) {
                String[] splitNames = fullName.split(" ");
                firstName = splitNames[0];
                lastName = splitNames[1];
            } else {
                firstName = fullName;
            }
            loginToServer(firstName, lastName, emailAddress, null, id);
        }
    };

    private final OnFailureListener firebaseAuthFailureListener = e -> {
        Log.w(TAG, "checkPending:onFailure", e);
        e.printStackTrace();
        Toast.makeText(AuthActivity.this, "Sign in with Apple: " + e.getMessage(), Toast.LENGTH_SHORT).show();
    };

    public static void createKeyHash(Activity activity, String yourPackage) {
        try {
            PackageInfo info = activity.getPackageManager().getPackageInfo(yourPackage, PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }
}