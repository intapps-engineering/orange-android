package com.zabihah.ui.presentation.ui.userprofile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.zabihah.ui.R;

public class EditUserProfileFragment extends Fragment {

    private EditUserProfileViewModel editUserProfileViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        editUserProfileViewModel =
                new ViewModelProvider(this).get(EditUserProfileViewModel.class);
        View root = inflater.inflate(R.layout.fragment_edit_user_profile, container, false);
        final TextView textView = root.findViewById(R.id.text_user_profile);

        return root;
    }
}