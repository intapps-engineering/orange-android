package com.zabihah.ui.presentation.ui.helpfaq;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class HelpandFaqViewModel extends ViewModel {
    private MutableLiveData<String> mText;

    public HelpandFaqViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is help and faq fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}
