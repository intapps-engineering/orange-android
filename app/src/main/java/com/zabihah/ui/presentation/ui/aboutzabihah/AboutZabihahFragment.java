package com.zabihah.ui.presentation.ui.aboutzabihah;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.zabihah.ui.R;

public class AboutZabihahFragment extends Fragment {
    private AboutZabihahViewModel aboutZabihahViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        aboutZabihahViewModel =
                new ViewModelProvider(this).get(AboutZabihahViewModel.class);
        View root = inflater.inflate(R.layout.fragment_about_zabihah, container, false);
        final TextView textView = root.findViewById(R.id.text_about_zabihah);

        return root;
    }
}
