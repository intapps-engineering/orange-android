package com.zabihah.ui.presentation.ui.privacypolicy;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.zabihah.ui.R;

public class PrivacyPolicyFragment extends Fragment {
    private PrivacyPolicyViewModel privacypolicyViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        privacypolicyViewModel =
                new ViewModelProvider(this).get(PrivacyPolicyViewModel.class);
        View root = inflater.inflate(R.layout.fragment_privacy_policy, container, false);
        final TextView textView = root.findViewById(R.id.text_privacy_policy);

        return root;
    }
}
