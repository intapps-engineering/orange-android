package com.zabihah.ui.presentation.ui.savedplaces;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class UserSavedPlacesViewModel extends ViewModel {
    private MutableLiveData<String> mText;

    public UserSavedPlacesViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is user saved places fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}
