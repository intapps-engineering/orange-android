package com.zabihah.ui.presentation.ui.addrecord;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.zabihah.ui.R;

public class AddRecordFragment extends Fragment {

    private AddRecordViewModel addRecordViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        addRecordViewModel =
                new ViewModelProvider(this).get(AddRecordViewModel.class);
        View root = inflater.inflate(R.layout.fragment_add_record, container, false);
        final TextView textView = root.findViewById(R.id.text_add_record);

        return root;
    }
}