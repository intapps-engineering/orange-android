package com.zabihah.ui.presentation.ui.savedplaces;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.zabihah.ui.R;

public class UserSavedPlacesFragment extends Fragment {

    private UserSavedPlacesViewModel userSavedPlacesViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        userSavedPlacesViewModel =
                new ViewModelProvider(this).get(UserSavedPlacesViewModel.class);
        View root = inflater.inflate(R.layout.fragment_user_saved_places, container, false);
        final TextView textView = root.findViewById(R.id.text_user_saved_places);

        return root;
    }
}