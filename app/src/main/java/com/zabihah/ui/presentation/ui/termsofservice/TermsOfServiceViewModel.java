package com.zabihah.ui.presentation.ui.termsofservice;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class TermsOfServiceViewModel extends ViewModel {
    private MutableLiveData<String> mText;

    public TermsOfServiceViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is terms of service fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}
