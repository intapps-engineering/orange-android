package com.zabihah.ui.presentation.ui.helpfaq;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.zabihah.ui.R;

public class HelpandFaqFragment extends Fragment {
    private HelpandFaqViewModel helpandFaqViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        helpandFaqViewModel =
                new ViewModelProvider(this).get(HelpandFaqViewModel.class);
        View root = inflater.inflate(R.layout.fragment_help_faq, container, false);
        final TextView textView = root.findViewById(R.id.text_help_faq);

        return root;
    }
}
