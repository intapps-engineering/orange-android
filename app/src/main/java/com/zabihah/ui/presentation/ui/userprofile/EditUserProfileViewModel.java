package com.zabihah.ui.presentation.ui.userprofile;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class EditUserProfileViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public EditUserProfileViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is Edit user profile fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}