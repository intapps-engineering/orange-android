package com.zabihah.ui.presentation.ui.termsofservice;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.zabihah.ui.R;

public class TermsOfServiceFragment extends Fragment {
    private TermsOfServiceViewModel termsOfServiceViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        termsOfServiceViewModel =
                new ViewModelProvider(this).get(TermsOfServiceViewModel.class);
        View root = inflater.inflate(R.layout.fragment_terms_of_service, container, false);
        final TextView textView = root.findViewById(R.id.text_terms_of_service);

        return root;
    }
}
