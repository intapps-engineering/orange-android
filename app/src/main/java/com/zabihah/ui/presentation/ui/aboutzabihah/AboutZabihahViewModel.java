package com.zabihah.ui.presentation.ui.aboutzabihah;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class AboutZabihahViewModel extends ViewModel {
    private MutableLiveData<String> mText;

    public AboutZabihahViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is about zabihah fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}
