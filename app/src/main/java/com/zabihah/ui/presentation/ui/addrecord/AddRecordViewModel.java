package com.zabihah.ui.presentation.ui.addrecord;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class AddRecordViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public AddRecordViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("");
    }
    public LiveData<String> getText() {
        return mText;
    }
}